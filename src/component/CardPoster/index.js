import React from 'react';
import { StyleSheet, View } from 'react-native';

function CardPoster(props) {
  // eslint-disable-next-line react/destructuring-assignment
  return <View style={{ ...styles.container, ...props.style }}>{props.children}</View>;
}

export default CardPoster;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    marginTop: 16,
    borderRadius: 4,
    overflow: 'hidden',
  },
});
