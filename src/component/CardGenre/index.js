import {
  StyleSheet, Text, View, Dimensions,
} from 'react-native';
import React from 'react';
import { colors, fonts } from '../../utils';

function CardGenre({ genres }) {
  return (
    <View style={styles.container}>
      <Text style={styles.genre}>{genres}</Text>
    </View>
  );
}

export default CardGenre;

const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  genre: {
    fontFamily: fonts.primary[600],
    color: colors.text.genre,
    fontSize: 14,
    textAlign: 'center',
  },
  container: {
    marginHorizontal: 5,
    backgroundColor: colors.background.genre,
    borderRadius: 10,
    height: windowHeight * 0.04,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
    paddingHorizontal: 10,
  },
});
