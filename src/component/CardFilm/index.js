import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Dimensions, Image, StyleSheet, Text, View,
} from 'react-native';
import { colors, fonts } from '../../utils';
import Button from '../Button';
import CardGenre from '../CardGenre';
import Loading from '../Loading';

function CardFilm({
  onPress, title, date, rating, genre, source,
}) {
  const [isLoading, setLoading] = useState(true);

  const [data, setData] = useState([]);
  useEffect(() => {
    async function getData(getGenre) {
      try {
        const response = await axios.get(`http://code.aldipee.com/api/v1/movies/${getGenre}`);
        if (isMounted) setData(response.data);
      } catch (error) {
        console.error('Error :', error);
      } finally {
        setLoading(false);
      }
    }
    let isMounted = true;
    getData(genre);
    return () => {
      isMounted = false;
    };
  }, [genre]);
  return (
    <View>
      {isLoading ? (
        <Loading />
      ) : (
        <View style={styles.container}>
          <Image source={source} style={styles.imageStyle} />
          <View style={styles.description}>
            <Text style={styles.titleFilm}>{title}</Text>
            <Text style={styles.dateRelease}>{date}</Text>
            <Text style={styles.rating}>
              {rating}
              /10
            </Text>
            <View style={styles.wrapperGenre}>
              {data.genres.map((item) => (
                <CardGenre key={item.id} genres={item.name} />
              ))}
            </View>
            <View style={{ position: 'absolute', bottom: 0 }}>
              <Button onPress={onPress} title="Show More" />
            </View>
          </View>
        </View>
      )}
    </View>

  );
}

export default CardFilm;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 16,
    width: windowWidth * 0.95,
    height: windowHeight * 0.3,
  },
  imageStyle: {
    width: null,
    height: null,
    borderRadius: 11,
    marginRight: 16,
    flex: 2,
  },
  description: { flex: 4 },
  titleFilm: {
    fontSize: 16,
    color: colors.text.primary,
    paddingBottom: 4,
    fontFamily: fonts.primary[600],
  },
  dateRelease: {
    color: colors.text.primary,
    fontSize: 12,
    paddingBottom: 4,
    fontFamily: fonts.primary[600],
  },
  rating: {
    color: colors.text.rating,
    fontSize: 12,
    paddingBottom: 4,
    fontFamily: fonts.primary[600],
  },

  genre: {
    color: colors.text.primary,
    fontSize: 12,
    paddingBottom: 4,
    fontFamily: fonts.primary[600],
  },

  wrapperGenre: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
