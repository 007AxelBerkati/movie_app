const mainColors = {
  black1: '#17082A',
  black2: '#000000',
  black3: 'rgba(33, 15, 55, 1)',
  black3Opacity: 'rgba(33, 15, 55, 0.85)',
  purple1: '#6644B8',
  orange1: '#F79E44',
  pink1: '#FF8FC6',
  pink2: '#ECBBDA',
  white2: 'rgba(255,255,255, 0.2)',
  grey4: '#B1B7C2',
};

export const colors = {
  background: {
    primary: mainColors.black1,
    secondary: 'white',
    icon: {
      normal: mainColors.black3,
      opacity: mainColors.black3Opacity,
    },
    genre: mainColors.white2,
  },

  text: {
    primary: 'white',
    secondary: mainColors.black2,
    rating: mainColors.orange1,
    genre: mainColors.pink2,
    description: mainColors.grey4,
  },
  button: {
    primary: {
      background: mainColors.purple1,
      text: 'white',
    },
    secondary: {
      background: mainColors.pink1,
      text: 'white',
    },
  },
};
