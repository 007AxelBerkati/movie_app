import axios from 'axios';

export default axios.create({
  baseURL: 'http://code.aldipee.com/api/v1/movies',
});
