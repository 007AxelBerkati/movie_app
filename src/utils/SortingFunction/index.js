export function latestSort(a, b) {
  return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
}

export function recommendedSort(a, b) {
  return parseFloat(b.popularity) - parseFloat(a.popularity);
}
