import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import NetInfo from '@react-native-community/netinfo';
import Router from './router';
import { NoInternet } from './component';

function App() {
  const [isOffline, setOfflineStatus] = useState(false);

  const removeNetInfo = () => {
    NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  }, []);

  return <NavigationContainer>{isOffline ? <NoInternet /> : <Router />}</NavigationContainer>;
}

export default App;
