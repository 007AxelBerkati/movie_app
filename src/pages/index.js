import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';
import SplashScreen from './SplashScreen';

export { HomeScreen, DetailScreen, SplashScreen };
