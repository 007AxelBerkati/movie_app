import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  RefreshControl,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Share from 'react-native-share';
import {
  CardDetail, CardPoster, IconButton, CardGenre, Loading,
} from '../../component';
import { colors, fonts } from '../../utils';
import MovieAPI from '../../utils/APIService/MovieAPI';

function DetailScreen({ route, navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const { id } = route.params;

  let isMounted = true;

  async function getData(getId) {
    try {
      await MovieAPI.get(`/${getId}`).then((response) => {
        if (isMounted && response.status === 200) {
          setData(response.data);
        } else {
          // eslint-disable-next-line no-undef
          Alert.alert('Error', 'Error to getData');
        }
      });
    } catch (error) {
      console.error(error);
      setFalse();
    } finally {
      setFalse();
    }
  }
  function onRefresh() {
    setRefreshing(true);
    getData(id);
  }

  async function myCustomShare() {
    const shareOptions = {
      message: `Go to homepage for film ${data.original_title} ${data.homepage}`,
    };

    try {
      await Share.open(shareOptions);
    } catch (error) {
      console.log('Error : ', error);
    }
  }

  const setFalse = () => {
    setLoading(false);
    setRefreshing(false);
  };

  useEffect(() => {
    getData(id);
    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      isMounted = false;
    };
  }, []);

  if (!data) {
    return null;
  }

  return (
    <View style={styles.page}>
      {isLoading ? (
        <Loading />
      ) : (
        <View>
          <StatusBar barStyle="light-content" backgroundColor={colors.background.primary} />
          <ScrollView
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => onRefresh} />}
          >
            <ImageBackground
              source={{ uri: data.backdrop_path }}
              style={styles.header}
              imageStyle={{ opacity: 0.5 }}
            >
              <View style={styles.iconButton}>
                <View style={{ flex: 3 }}>
                  <IconButton type="Back" onPress={() => navigation.goBack()} />
                </View>
                <View
                  style={{
                    flex: 2,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <View style={{ paddingLeft: 40 }}>
                    <IconButton type="Like" />
                  </View>
                  <View>
                    <IconButton type="Share" onPress={() => myCustomShare()} />
                  </View>
                </View>
              </View>
            </ImageBackground>
            <CardDetail
              title={data.title}
              voteAverage={data.vote_average}
              status={data.status}
              date={data.release_date}
              runtime={data.runtime}
              tagline={data.tagline}
              source={{ uri: data.poster_path }}
            />
            <View style={styles.bodyWrapper}>
              <Text style={styles.label}>Genres</Text>
              <View style={styles.genreSection}>
                {data.genres.map((item) => (
                  <CardGenre key={item.id} genres={item.name} />
                ))}
              </View>
              <Text style={styles.label}>Synopsis</Text>
              <View style={styles.synopsisSection}>
                <Text style={styles.synopsis}>{data.overview}</Text>
              </View>
              <Text style={styles.label}>Actor/Artist</Text>
              <View style={styles.actorSection}>
                {data.credits.cast.map((item) => {
                  let test = '';
                  if (item.profile_path === 'https://image.tmdb.org/t/p/w500null') {
                    test = 'https://www.tech101.in/wp-content/uploads/2018/07/xblank-profile-picture-300x300.png.pagespeed.ic.vr0p7Y9_y4.webp';
                  } else {
                    test = item.profile_path;
                  }
                  return (
                    <CardPoster key={item.id} style={styles.cardActor}>
                      <Image source={{ uri: test }} style={styles.image} />
                      <Text style={styles.name}>{item.name}</Text>
                    </CardPoster>
                  );
                })}
              </View>
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
}

export default DetailScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
  },
  bodyWrapper: {
    marginHorizontal: 13,
  },
  genreSection: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  label: {
    marginTop: 18,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    fontSize: 20,
    marginBottom: 10,
  },

  genre: {
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    fontSize: 14,
    marginRight: 10,
  },

  synopsis: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
    textAlign: 'justify',
  },

  actorSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  iconButton: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginHorizontal: 13,
    marginVertical: 13,
  },

  cardActor: {
    height: windowHeight * 0.29,
    width: windowWidth * 0.29,
  },

  image: {
    height: '90%',
    width: null,
    resizeMode: 'contain',
  },
  name: {
    fontSize: 12,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    textAlign: 'center',
    paddingTop: 5,
  },
});
