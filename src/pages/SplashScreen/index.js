import {
  StyleSheet, Text, View, Image, StatusBar, Dimensions,
} from 'react-native';
import React, { useEffect } from 'react';
import { LogoApp } from '../../assets';
import { colors, fonts } from '../../utils';

function SplashScreen({ navigation }) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('HomeScreen');
    }, 3000);
  });
  return (
    <View style={styles.page}>
      <StatusBar barStyle="light-content" backgroundColor={colors.background.primary} />
      <Image style={styles.image} source={LogoApp} />
      <Text style={styles.title}>Movies Stream</Text>
      <Text style={styles.nickname}>Axel Berkati</Text>
    </View>
  );
}

export default SplashScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.background.primary,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    color: colors.text.rating,
    fontFamily: fonts.primary[800],
  },

  image: {
    height: windowHeight * 0.17,
    width: windowWidth * 0.3,
  },

  nickname: {
    fontSize: 14,
    color: colors.text.primary,
    fontFamily: fonts.primary[800],
    position: 'absolute',
    bottom: 19,
  },
});
