import React, { useEffect, useState } from 'react';
import {
  Alert,
  Dimensions,
  Image,
  RefreshControl,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
// import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { CardFilm, CardPoster, Loading } from '../../component';
import {
  colors, fonts, latestSort, recommendedSort,
} from '../../utils';
import MovieAPI from '../../utils/APIService/MovieAPI';

function HomeScreen({ navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  let isMounted = true;

  async function getData() {
    try {
      await MovieAPI.get().then((response) => {
        if (isMounted && response.status === 200) {
          setData(response.data.results);
        } else {
          Alert.alert('Error to getData');
        }
      });
    } catch (err) {
      console.error(err);
      setFalse();
    } finally {
      setFalse();
    }
  }
  function onRefresh() {
    setRefreshing(true);
    getData();
  }

  const setFalse = () => {
    setLoading(false);
    setRefreshing(false);
  };

  useEffect(() => {
    getData();

    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      isMounted = false;
    };
  }, []);

  return (
    <View View style={styles.page}>
      {isLoading ? (
        <Loading />
      ) : (
        <View>
          <StatusBar barStyle="light-content" backgroundColor={colors.background.primary} />
          <ScrollView
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => onRefresh} />}
          >
            <Text style={styles.label}>Recommended</Text>
            <View style={styles.wrapperScroll}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={styles.recommended}>
                  {data.sort(recommendedSort).map((item) => (
                    <CardPoster key={item.id} style={styles.cardFilm}>
                      <TouchableOpacity
                        onPress={() => navigation.navigate('DetailScreen', { id: item.id })}
                      >
                        <Image source={{ uri: item.poster_path }} style={styles.image} />
                      </TouchableOpacity>
                    </CardPoster>
                  ))}
                </View>
              </ScrollView>
            </View>
            <Text style={styles.label}>Latest Upload</Text>
            <View style={styles.wrapperSections}>
              {data.sort(latestSort).map((item) => (
                <CardFilm
                  key={item.id}
                  title={item.title}
                  rating={item.vote_average}
                  date={item.release_date}
                  genre={item.id}
                  source={{ uri: item.poster_path }}
                  onPress={() => navigation.navigate('DetailScreen', { id: item.id })}
                />
              ))}
            </View>
          </ScrollView>
        </View>
      )}
    </View>

  );
}

export default HomeScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    paddingVertical: 13,
    paddingHorizontal: 13,
  },

  label: {
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    fontSize: 20,
  },

  recommended: {
    flexDirection: 'row',
    marginBottom: 16,
  },

  wrapperSections: {
    justifyContent: 'space-between',
  },
  image: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
  },
  cardFilm: {
    marginRight: 15,
  },
});
